package net.categoricaldata.category

import net.categoricaldata.universalalgebra._

trait FinitelyPresentedCategories extends Category with Products {
  override type O = FinitelyPresentedCategory
  override type M = functor.withFinitelyPresentedSource.withFinitelyPresentedTarget
}

trait CoproductsForFinitelyPresentedCategories extends Coproducts { category: FinitelyPresentedCategories =>
  override def coproduct(xs: FinitelyPresentedCategory*): FinitelyPresentedCategory = {
    new FinitelyPresentedCategory {
      type xO = Any
      type xG = Any
      override type O = (Int, xO)
      override type G = (Int, xG)
      override def objectsAtLevel(k: Int): List[O] = {
        for ((x, i) <- xs.toList.zipWithIndex; o <- x.objectsAtLevel(k)) yield (i, o.asInstanceOf[xO])
      }
      override def generatorSource(g: G) = {
        val x = xs(g._1)
        (g._1, x.generatorSource(g._2.asInstanceOf[x.G]))
      }
      override def generatorTarget(g: G) = {
        val x = xs(g._1)
        (g._1, x.generatorTarget(g._2.asInstanceOf[x.G]))
      }
      override def generators(source: O, target: O) = {
        if (source._1 == target._1) {
          val x = xs(source._1)
          val sx = source._2.asInstanceOf[x.O]
          val tx = target._2.asInstanceOf[x.O]
          for (g <- x.generators(sx, tx)) yield (source._1, g)
        } else {
          Nil
        }
      }
      override def relations(source: O, target: O) = {
        if (source._1 == target._1) {
          val x = xs(source._1)
          val sx = source._2.asInstanceOf[x.O]
          val tx = target._2.asInstanceOf[x.O]
          for ((r1, r2) <- x.relations(sx, tx)) yield {
            (
              Path(source, target, for (g <- r1.morphisms) yield {
                (source._1, g.asInstanceOf[xG])
              }),
              Path(source, target, for (g <- r1.morphisms) yield {
                (source._1, g.asInstanceOf[xG])
              }))
          }
        } else {
          Nil
        }
      }
      override val maximumLevel = xs.map(_.maximumLevel).max
      override val minimumLevel = xs.map(_.minimumLevel).min
      override def pathHashCode(path: Path) = ???
      override def pathEquality(path1: Path, path2: Path) = ???
      override val functorsToSet = ???
    }
  }
  override def coproductInjections(xs: FinitelyPresentedCategory*): List[M] = ???
  override def coproductUniversality(o: FinitelyPresentedCategory, ms: List[M]): M = ???
}

trait ProductsForFinitelyPresentedCategories extends Products { category: FinitelyPresentedCategories =>
  override def product(xs: FinitelyPresentedCategory*): FinitelyPresentedCategory = {
    new FinitelyPresentedCategory {
      type xO = Any
      type xG = Any
      override type O = List[xO]
      override type G = (O, O, Int, xG)
      override def objectsAtLevel(k: Int): List[O] = {
        ???
      }
      override def generatorSource(g: G) = g._1
      override def generatorTarget(g: G) = g._2
      override def generators(source: O, target: O) = {
        if (source == target) {
          (for ((s, i) <- source.zipWithIndex) yield {
            val x = xs(i)
            val sx = s.asInstanceOf[x.O]
            for (g <- x.generators(sx, sx)) yield {
              (source, source, i, g)
            }
          }).flatten
        } else {
          if (source.zip(target).count(p => p._1 == p._2) == 1) {
            val i = source.zip(target).indexWhere(p => p._1 == p._2)
            val x = xs(i)
            val sx = source(i).asInstanceOf[x.O]
            val tx = target(i).asInstanceOf[x.O]
            for (g <- x.generators(sx, tx)) yield {
              (source, target, i, g)
            }
          } else {
            Nil
          }
        }
      }
      override def relations(source: O, target: O) = {
        ???
      }
      override val maximumLevel = xs.map(_.maximumLevel).sum
      override val minimumLevel = xs.map(_.minimumLevel).sum
      override def pathHashCode(path: Path) = ???
      override def pathEquality(path1: Path, path2: Path) = ???
      override val functorsToSet = ???
    }
  }
  override def productProjections(xs: FinitelyPresentedCategory*): List[M] = ???
  override def productUniversality(o: FinitelyPresentedCategory, ms: List[M]): M = ???
}

trait EqualizersForFinitelyPresentedCategories extends EqualizersAreSubs { category: FinitelyPresentedCategories =>
  override def equalizerObject(ms: category.M*): category.O = ???
}

trait CoequalizersForFinitelyPresentedCategories extends Coequalizers { category: FinitelyPresentedCategories =>
  override def coequalizerMorphism(ms: category.M*): category.M = ???
  override def coequalizerObject(ms: category.M*): category.O = category.target(coequalizerMorphism(ms: _*))
  override def coequalizerUniversality(m: category.M, ms: category.M*): category.M = ???
}

object FinitelyPresentedCategories extends FinitelyPresentedCategories with ProductsForFinitelyPresentedCategories with CoproductsForFinitelyPresentedCategories {
  override def source(m: M) = m.source
  override def target(m: M) = m.target
  override def compose(m1: M, m2: M): M = ???
  override def identity(o: O) = o.identityFunctor
}