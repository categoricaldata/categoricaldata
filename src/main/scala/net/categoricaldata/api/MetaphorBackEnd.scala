package net.categoricaldata.api

import net.categoricaldata.server.transformers.DatasetTransformer
import net.categoricaldata.server.transformers.TranslationTransformer
import net.categoricaldata.server.json.PrettyPrinter
import net.categoricaldata.server.json.Pack

object MetaphorBackEnd extends BackEnd {
  val datasetTransformer = new DatasetTransformer
  val translationTransformer = new TranslationTransformer

  override def readme = "Metaphor's implementation of the back end, for comparisons with FQL."
  override def version = "metaphor 0.1.0"
  override def iso(instance1: String, instance2: String) = {
    val x1 = datasetTransformer.toValue(instance1).get
    val x2 = datasetTransformer.toValue(instance2).get
    val C = x1.source
    val y1 = C.internalize(x1)
    val y2 = C.internalize(x2)
    if(y1.isIsomorphicTo(y2)) {
      "yes"
    } else { 
      "no"
    }
  }
  override def pi(instance: String, functor: String) = {
    val F = translationTransformer.toValue(functor).get
    val x = datasetTransformer.toValue(instance).get
    val result = F.rightPushforward.onObjects(F.source.internalize(x))
    PrettyPrinter(Pack.packDataset(result))
  }
  override def sigma(instance: String, functor: String) =  {
    val F = translationTransformer.toValue(functor).get
    val x = datasetTransformer.toValue(instance).get
    val result = F.leftPushforward.onObjects(F.source.internalize(x))
    PrettyPrinter(Pack.packDataset(result))
  }
  override def delta(instance: String, functor: String) =  {
    val F = translationTransformer.toValue(functor).get
    val x = datasetTransformer.toValue(instance).get
    val result = F.pullback.onObjects(F.target.internalize(x))
    PrettyPrinter(Pack.packDataset(result))
  }
}