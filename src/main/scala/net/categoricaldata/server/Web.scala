package net.categoricaldata.server

import org.mortbay.jetty.Connector
import org.mortbay.jetty.Server
import org.mortbay.jetty.webapp.WebAppContext
import org.mortbay.jetty.nio._
import scala.util.Properties
import org.mortbay.jetty.handler.ResourceHandler
import org.mortbay.jetty.handler.HandlerList
import org.mortbay.jetty.Handler
import org.mortbay.jetty.handler.ContextHandler
import scala.collection.JavaConverters

object Web extends App {
  val server = new Server
  val scc = new SelectChannelConnector
  scc.setPort(Properties.envOrElse("PORT", "8080").toInt)
  server.setConnectors(Array(scc))

  val metaphorContext = new WebAppContext()
  metaphorContext.setServer(server)
  metaphorContext.setContextPath("/metaphor")
  metaphorContext.setWar("./src/main/webapp")
  server.addHandler(metaphorContext)

  val staticContext = new WebAppContext()
  staticContext.setDefaultsDescriptor("./src/site/webdefault.xml"); // enable symlinks, by (ridiculously) having our own version of webdefault with a few extra lines
  staticContext.setContextPath("/")
  staticContext.setResourceBase("./src/site");
  server.addHandler(staticContext)
  
  try {
    println(">>> STARTING EMBEDDED JETTY SERVER, PRESS ANY KEY TO STOP")
    server.start()
    while (System.in.available() == 0) {
      Thread.sleep(5000)
    }
    server.stop()
    server.join()
  } catch {
    case exc: Exception => {
      exc.printStackTrace()
      System.exit(100)
    }
  }
}
