package net.categoricaldata.ontology
import net.categoricaldata.category._
import net.categoricaldata.sets._
import net.categoricaldata.dsl.Sentences

trait Translation extends functor.withFinitelyPresentedSource.withFinitelyPresentedTarget { translation =>
  override val source: Ontology
  override val target: Ontology

  def toJSON = net.categoricaldata.server.json.Pack.packTranslation(this)

  override def equals(other: Any): Boolean = {
    other match {
      case other: Translation => {
        if (source != other.source) return false
        if (target != other.target) return false
        for (o <- source.objects) {
          if (this.onObjects(o) != other.onObjects(o)) return false
        }
        for (g <- source.allGenerators) {
          if (this.onGenerators(g) != other.onGenerators(g)) return false
        }
        true
      }
      case _ => false
    }
  }

  override def toString = {
    "Translation(\n" +
      "  source = " + source.toString + ",\n" +
      "  target = " + target.toString + ",\n" +
      "  onObjects = " + source.objects.map(o => o -> this.onObjects(o)).toMap.toString + ",\n" +
      "  onMorphisms = " + source.allGenerators.map(g => g -> this.onGenerators(g)).toMap.toString + ")"
  }

  private class FiniteSliceCategory(onRight: translation.target.O) extends SliceCategory(
    translation.target.asInstanceOf[Ontology.Finite].maximumWordLength,
    onRight) with LocallyFinitelyGeneratedCategory.CachingGenerators

  private class FiniteCosliceCategory(onLeft: translation.target.O) extends CosliceCategory(
    translation.target.asInstanceOf[Ontology.Finite].maximumWordLength,
    onLeft) with LocallyFinitelyGeneratedCategory.CachingGenerators

  class SliceFunctor extends super.SliceFunctor {
    import net.tqft.toolkit.functions.Memo
    override val buildSliceCategory: Box => SliceCategory = Memo({ onRight: Box => new FiniteSliceCategory(onRight) })
  }
  class CosliceFunctor extends super.CosliceFunctor {
    import net.tqft.toolkit.functions.Memo
    override val buildCosliceCategory: Box => CosliceCategory = Memo({ onLeft: Box => new FiniteCosliceCategory(onLeft) })
  }

  lazy val slice: SliceFunctor = new SliceFunctor
  lazy val coslice: CosliceFunctor = new CosliceFunctor

  //  trait PullbackLeftAdjoint extends LeftAdjoint { self: Functor =>
  //    lazy val rightAdjoint = pullback
  //    lazy val rightUnit = rightAdjoint.leftUnit
  //    lazy val rightCounit = rightAdjoint.leftCounit
  //  }
  //  trait PullbackRightAdjoint extends RightAdjoint { self: Functor =>
  //    lazy val leftAdjoint = pullback
  //    lazy val leftUnit = leftAdjoint.rightUnit
  //    lazy val leftCounit = leftAdjoint.rightCounit
  //  }

  trait CovariantDataFunctor extends super.CovariantDataFunctor {
    override val target: translation.target.functorsToSet.type = translation.target.functorsToSet
  }

  trait RightPushforward extends CovariantDataFunctor //with PullbackRightAdjoint 
  { pushforward =>
    val CSet = source
    val DSet = target
    val F: translation.type = translation
    val C: F.source.type = F.source
    val D: F.target.type = F.target
    override def onObjects(i: source.O): target.O = (new D.Dataset { //i is a dataset on C, we're going to define a dataset F_*(i) on D.
      type DObject = Box
      type DArrow = Arrow
      override def onObjects(o: Box) = {
        val F = slice.onObjects(o)
        F.pullback.onObjects(i).limitSet //We have now defined F_*(i)(o)
      }
      override def onGenerators(g: translation.target.G) = {
        val Dop: D.opposite.type = D.opposite
        val gop = Dop.reverseGenerator(g)
        val sg = slice.onMorphisms(Dop.generatorAsMorphism(gop))
        val Fg = sg.functor
        val Ft = sg.target

        Fg.pullback.limitMorphism(Ft.pullback.onObjects(i).asInstanceOf[Fg.target.FunctorToSet])

      }
    }).memo
    override def onMorphisms(m: CSet.M): DSet.M = new D.Datamap {
      override val source = pushforward.onObjects(CSet.internalize(m.source)) // TODO replace these with CSet.source etc
      override val target = pushforward.onObjects(CSet.internalize(m.target))
      override def apply(d: Box): FFunction = {
        val pi = slice.onObjects(d) //pi: (d|F)-->C
        val `pi^*(m)` = pi.pullback.onMorphisms(m) //pi^*(m): pi^*(i)-->pi^*(j)
        `pi^*(m)`.limitFunction //lim_{d|F}pi^*(m)
      }
    }
  }

  trait LeftPushforward extends CovariantDataFunctor /* with PullbackLeftAdjoint */ { shriek => // left pushforward of translation is a functor from C-sets to D-sets
    val CSet: source.type = source
    val DSet = target
    val F: translation.type = translation //we need to be extra careful here, not just writing val F=translation, because we need to tell the type system exactly what's going on.
    val C: F.source.type = F.source
    val D: F.target.type = F.target
    override def onObjects(i: CSet.O): DSet.O = (new D.Dataset { //i is a dataset on C, we're going to define a dataset F_!(i) on D.
      type DObject = Box
      type DArrow = Arrow
      override def onObjects(o: DObject) = {
        val cs = coslice.onObjects(o) // Weird, why on earth do we need this intermediate val?
        cs.pullback.onObjects(i).colimitSet //We have now defined F_!(i)(o)
      }
      override def onGenerators(g: DArrow) = {
        val sg = coslice.onMorphisms(D.generatorAsMorphism(g)) // a commutative triangle (F|o) --> (F|p) --> C
        val Fg = sg.functor // (F|o) --> (F|p)
        val Ft = sg.target // (F|p) --> C
        Fg.pullback.colimitMorphism(Ft.pullback.onObjects(i).asInstanceOf[Fg.target.FunctorToSet])
      }

    }).memo // memo on a dataset makes sure that we never have to do the same computation twice.

    def renamingIsomorphism(i: CSet.O): DSet.M = {
      def f(o: Box): FFunction = {
        // FIXME: what if o doesn't have a preimage? what if it has several?
        val p = translation.source.objects.find(p => translation.onObjects(p) == o).get
        
        leftUnit(i)(p) match {
          case g if g.isomorphism_? => g.inverse
          case _ => shriek.onObjects(i).onObjects(o).identity
        }
      }

      shriek.onObjects(i).constructDataIsomorphismFromComponents(f)
    }

    override def onMorphisms(m: CSet.M): DSet.M = new D.Datamap {
      override val source = onObjects(CSet.internalize(m.source)) // F_!(i)
      override val target = onObjects(CSet.internalize(m.target)) // F_!(j)
      override def apply(d: Box): FFunction = {
        val pi = coslice.onObjects(d) // pi: (F|d)-->C
        val `pi^*(m)` = pi.pullback.onMorphisms(m) //pi^*(m): pi^*(i)-->pi^*(j)
        `pi^*(m)`.colimitFunction // colim_{F|d} pi^*(m)
      }
    }
  }

  private trait MemoLeftPushforward extends LeftPushforward with Functor.MemoFunctor
  private trait MemoRightPushforward extends RightPushforward with Functor.MemoFunctor

  lazy val leftPushforward: LeftPushforward = new MemoLeftPushforward {}
  lazy val rightPushforward: RightPushforward = new MemoRightPushforward {}

  def __! : ParametrizedFunctor[Datasets.type, translation.target.functorsToSet.type] = {
    new ParametrizedFunctor[Datasets.type, translation.target.functorsToSet.type] {
      override val source = Datasets
      override val target = translation.target.functorsToSet
      def onObjects(i: Dataset) = target.internalize(leftPushforward.onObjects(leftPushforward.source.internalize(i)))
      def onMorphisms(t: Datamap) = target.internalize(leftPushforward.onMorphisms(leftPushforward.source.internalize(t)))
    }
  }
  def __* : ParametrizedFunctor[Datasets.type, translation.target.functorsToSet.type] = {
    new ParametrizedFunctor[Datasets.type, translation.target.functorsToSet.type] {
      override val source = Datasets
      override val target = translation.target.functorsToSet
      def onObjects(i: Dataset) = target.internalize(rightPushforward.onObjects(rightPushforward.source.internalize(i)))
      def onMorphisms(t: Datamap) = target.internalize(rightPushforward.onMorphisms(rightPushforward.source.internalize(t)))
    }
  }

  trait ContravariantDataFunctor extends super.ContravariantDataFunctor {
    override val target: translation.source.Datasets.type = translation.source.Datasets
  }

  trait Pullback extends super.Pullback with ContravariantDataFunctor {
    /*
     *	F: C --> D
     *
     * 	F^*: DSet --> CSet, has
     * 		left adjoint F_!: C-Sets --> D-Sets
     * 		right adjoint F_*: C-Sets --> D-Sets
     * 
     * We have a (left) counit F_! F^* --> id_{C-Sets}
     *     and a (left) unit   id_{D-Sets} --> F^* F_!
     *     
     * and also a (right) counit F^* F_* --> id_{D-Sets}
     *      and a (right) unit   id_{C-Sets} --> F_* F^*
     */
    lazy val leftAdjoint = leftPushforward
    lazy val rightAdjoint = rightPushforward
  }

  override lazy val pullback: Pullback = new Pullback {}

  type SourceAdjunction = ParametrizedNaturalTransformation[translation.source.functorsToSet.type, translation.source.functorsToSet.type]
  type TargetAdjunction = ParametrizedNaturalTransformation[translation.target.functorsToSet.type, translation.target.functorsToSet.type]

  lazy val leftUnit: SourceAdjunction = new SourceAdjunction { leftUnit => // eta: id-->F^*F_!
    val F: translation.type = translation
    val C: F.source.type = F.source
    val D: F.target.type = F.target
    val `C-Set`: pullback.target.type = pullback.target
    override val source = `C-Set`.identityFunctor
    override val target = Functor.compose(leftPushforward, pullback)
    override def apply(i: `C-Set`.O): `C-Set`.M = {
      `C-Set`.internalize(new NaturalTransformationToSet {
        override val source = i
        override val target = leftUnit.target.onObjects(i)
        override def apply(c: C.O): FFunction = {
          val d = F.onObjects(c)
          val pi = coslice.onObjects(d) // pi:(F|d)-->C
          val `(F|d)`: pi.source.type = pi.source
          val `pi^*i` = pi.pullback.onObjects(i)
          val `id_d` = `(F|d)`.ObjectLeftOf(c, D.identity(d))
          `pi^*i`.colimitCoCone.functionToTerminalSet(`id_d`)
        }
      })
    }
  }

  /*
   * Given a functor F: C-->D.
   * Given a dataset i: C-->Set
   * Want: eta: i --> F^* F_! (i) 
   * Want: for each object c in C, a function eta(c): i(c)-->F^* F_! i(c)
   * Let d = F(c)
   * Check: F^* F_! i(c) = F_! i(d) = colim_{Fx-->d in (F|d)} i(x). 
   * Then eta is simply the structure map (in the cocone) with for the identity map (x=c and id_d: Fx-->d).
   * 
   */

  lazy val rightCounit: SourceAdjunction = new SourceAdjunction { rightCounit => //epsilon: F^*F_*-->id
    val F: translation.type = translation // F: C-->D
    val C: F.source.type = F.source
    val D: F.target.type = F.target
    val `C-Set` = pullback.target
    override val source = Functor.compose(rightPushforward, pullback)
    override val target = rightPushforward.source.identityFunctor
    override def apply(i: `C-Set`.O): `C-Set`.M = { //i: C-->Set
      `C-Set`.internalize(new NaturalTransformationToSet {
        override val source = rightCounit.source.onObjects(i)
        override val target = i
        override def apply(c: C.O): FFunction = {
          val d = F.onObjects(c)
          val pi = slice.onObjects(d) // pi: (d|F)-->C
          val `(d|F)`: pi.source.type = pi.source
          val `pi^*i` = pi.pullback.onObjects(i)
          val `id_d` = `(d|F)`.ObjectRightOf(c, D.identity(d))
          `pi^*i`.limitCone.functionFromInitialSet(`id_d`)
        }
      })
    }
  }

  /*
   * Given a functor F: C-->D.
   * Given a dataset i: C-->Set
   * Want: epsilon: F^* F_* (i) --> i 
   * Want: for each object c in C, a function epsilon(c): F^* F_* i(c) --> i(c)
   * Let d = F(c)
   * Check: F^* F_* i(c) = F_* i(d) = lim_{d-->Fx in (d|F)} i(x). 
   * Then eta is simply the projection map (in the cone) with for the identity map (x=c and id_d: Fx-->d).
   * 
   */

  lazy val leftCounit: TargetAdjunction = new TargetAdjunction { leftCounit => //epsilon: F_!F^*-->id
    val F: translation.type = translation
    val C: F.source.type = F.source
    val D: F.target.type = F.target
    val `D-Set` = pullback.source
    override val source = Functor.compose(pullback, leftPushforward) //F_!F^* : D-Set-->D-Set
    override val target = `D-Set`.identityFunctor //Id_{D-Set}: D-Set-->D-Set
    override def apply(i: `D-Set`.O): `D-Set`.M = { //i is a D-Set
      `D-Set`.internalize(new NaturalTransformationToSet {
        override val source = leftCounit.source.onObjects(i) //F_!F^*(i)  
        override val target = i //i
        override def apply(d: D.O): FFunction = { //d in D
          val pi = coslice.onObjects(d) //pi: (F|d)-->C
          val `(F|d)`: pi.source.type = pi.source
          val `pi^*F^*i` = pi.pullback.onObjects(F.pullback.onObjects(i))
          val `m(d)` = new `pi^*F^*i`.CoCone {
            override val terminalSet = i.onObjects(d)
            override def functionToTerminalSet(o: `(F|d)`.O) = new coConeFunction(o) {
              override def toFunction = {
                i.onMorphisms(o.morphism).toFunction
              }
            }
          }
          `pi^*F^*i`.colimitFunction(`m(d)`)
        }
        /* 
    * Given a functor F: C-->D. 
    * Given a dataset i: D-->Set
    * Want: epsilon: F_!F^*(i) --> i.
    * Want: for each object d in D, a function epsilon(d): F_!F^*(i)(d) --> i(d)
    * Let pi: (F | d)-->C.
    * Want: a function epsilon(d): colim_{Fc-->d in (F|d)} i(F(c)) --> i(d) 
    * Have a natural transformation m(d):pi^*F^*i-->{i(d)} of functors (F|d)-->Set, where {i(d)} is the constant functor.
    * Set epsilon(d) = colim {F|d} m(d).
    */
      })
    }
  }

  lazy val rightUnit: TargetAdjunction = new TargetAdjunction { rightUnit => //eta: id-->F_*F^*
    val F: translation.type = translation
    val C: F.source.type = F.source
    val D: F.target.type = F.target
    val `D-Set` = pullback.source
    override val source = pullback.source.identityFunctor
    override val target = Functor.compose(pullback, rightPushforward)
    override def apply(i: `D-Set`.O): targetCategory.M = {
      targetCategory.internalize(new NaturalTransformationToSet {
        override val source = i
        override val target = rightUnit.target.onObjects(i)
        override def apply(d: D.O): FFunction = {
          val pi = slice.onObjects(d) // pi: (d|F)-->C
          val `(d|F)`: pi.source.type = pi.source
          val `pi^*F^*i` = pi.pullback.onObjects(F.pullback.onObjects(i))
          val `m(d)` = new `pi^*F^*i`.Cone {
            override val initialSet = i.onObjects(d)
            override def functionFromInitialSet(o: `(d|F)`.O) = new coneFunction(o) {
              override def toFunction = {
                i.onMorphisms(o.morphism).toFunction
              }
            }
          }
          `pi^*F^*i`.limitFunction(`m(d)`)
        }
      })
    }
    /* 
    * Given a functor F: C-->D. 
    * Given a dataset i: D-->Set
    * Want: eta: i --> F_* F^* i.
    * Want: for each object d in D, a function eta(d): i(d) --> F_* F^* i(d)
    * Let pi: (d | F)-->C.
    * Want: a function eta(d): i(d) --> lim_{d-->Fc in (d|F)} i(F(c)) 
    * Have a cone m(d):{i(d)} --> pi^*F^*i of functors (d|F)-->Set, where {i(d)} is the constant functor.
    * Set eta(d) = lim {d|F} m(d).
    */
  }

  lazy val ^* : ParametrizedFunctor[Datasets.type, translation.source.functorsToSet.type] = {
    new ParametrizedFunctor[Datasets.type, translation.source.functorsToSet.type] {
      override val source = Datasets
      override val target = translation.source.functorsToSet
      def onObjects(i: Dataset) = target.internalize(pullback.onObjects(pullback.source.internalize(i)))
      def onMorphisms(t: Datamap) = target.internalize(pullback.onMorphisms(pullback.source.internalize(t)))
    }
  }

  def opposite: Translation = new Translation {
    override val source = translation.source.opposite
    override val target = translation.target.opposite
    override def onObjects(o: source.O) = translation.onObjects(o)
    override def onGenerators(g: source.G) = ???
  }

  def asPartialDataset: target.PartialDataset = new target.PartialDataset {
    override val source = translation.source
    override def onObjects(o: Box) = translation.onObjects(o)
    override def onGenerators(g: Arrow) = translation.onGenerators(g)
  }

}

object Translation {
  class ConcreteTranslation(override val source: Ontology, override val target: Ontology, onObjects: String => String, onMorphisms: Sentences.StringArrow => Sentences.StringPath, _json: Option[String] = None) extends Translation {
    private val objectMap: Map[Box, Box] = (for (s <- source.objects) yield {
      val t = target.objects.find(_.name == onObjects(s.name)).get
      s -> t
    }).toMap
    private val morphismMap: Map[Arrow, target.M] = (for (
      a <- source.allGenerators
    ) yield {
      val morphisms = for (Sentences.StringArrow(ts, to, tp) <- onMorphisms(Sentences.StringArrow(a.source.name, a.target.name, a.name)).arrows) yield {
        target.generatorAsMorphism(target.allGenerators.find(a => a.source.name == ts && a.name == tp && a.target.name == to).get)
      }
      a -> target.compose(objectMap(source.generatorSource(a)), morphisms)
    }).toMap

    verifyRelations

    override def onObjects(o: Box) = objectMap(o)
    // And again, replacing source.G with the apparently equivalent Arrow causes an AbstractMethodError
    override def onGenerators(a: source.G) = morphismMap(a)

    override def toJSON = super.toJSON.copy(json = _json)

  }

  def apply(source: Ontology, target: Ontology, onObjects: String => String, onMorphisms: Sentences.StringArrow => Sentences.StringPath, json: Option[String] = None): Translation = {
    // construct a new translation object
    new ConcreteTranslation(source, target, onObjects, onMorphisms, json)
  }

}
