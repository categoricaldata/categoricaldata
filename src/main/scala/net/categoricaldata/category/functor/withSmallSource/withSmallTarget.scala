package net.categoricaldata.category.functor.withSmallSource
import net.categoricaldata.category._

trait withSmallTarget extends Functor.withSmallSource with Functor.withSmallTarget { smallFunctor =>
  trait ContravariantDataFunctor extends ParametrizedFunctor[smallFunctor.target.D, smallFunctor.source.D] {
    override val source: smallFunctor.target.functorsToSet.type = smallFunctor.target.functorsToSet
    override val target: smallFunctor.source.functorsToSet.type = smallFunctor.source.functorsToSet
    def andThen(g: CovariantDataFunctor) = DataFunctors.compose(this, g)
  }
  trait CovariantDataFunctor extends ParametrizedFunctor[smallFunctor.source.D, smallFunctor.target.D] {
    override val source: smallFunctor.source.functorsToSet.type = smallFunctor.source.functorsToSet
    override val target: smallFunctor.target.functorsToSet.type = smallFunctor.target.functorsToSet
    def andThen(g: ContravariantDataFunctor) = DataFunctors.compose(this, g)
  }

  // TODO combine these two implementations, move them upwards?
  private object DataFunctors {
    def compose(f: ContravariantDataFunctor, g: CovariantDataFunctor): ParametrizedFunctor[f.source.type, g.target.type] = {
      new ParametrizedFunctor[f.source.type, g.target.type] {
        override val source: f.source.type = f.source
        override val target: g.target.type = g.target
        override def onObjects(o: source.O): target.O = g.onObjects(g.source.internalize(f.onObjects(o)))
        override def onMorphisms(m: source.M): target.M = g.onMorphisms(g.source.internalize(f.onMorphisms(m)))
      }
    }
    def compose(f: CovariantDataFunctor, g: ContravariantDataFunctor): ParametrizedFunctor[f.source.type, g.target.type] = {
      new ParametrizedFunctor[f.source.type, g.target.type] {
        override val source: f.source.type = f.source
        override val target: g.target.type = g.target
        override def onObjects(o: source.O): target.O = g.onObjects(g.source.internalize(f.onObjects(o)))
        override def onMorphisms(m: source.M): target.M = g.onMorphisms(g.source.internalize(f.onMorphisms(m)))
      }
    }
  }

  trait Pullback extends ContravariantDataFunctor { pullback =>
    override def onObjects(i: pullback.source.O) = pullback.target.internalize(new smallFunctor.source.FunctorToSet {
      def onObjects(o: smallFunctor.source.O) = i.onObjects(smallFunctor.onObjects(o))
      def onMorphisms(m: smallFunctor.source.M) = i.onMorphisms(smallFunctor.onMorphisms(m))
    })
    override def onMorphisms(m: pullback.source.M) = pullback.target.internalize(new smallFunctor.source.NaturalTransformationToSet {
      val source = onObjects(pullback.source.internalize(m.target))
      val target = onObjects(pullback.source.internalize(m.source))
      def apply(o: smallFunctor.source.O) = m(smallFunctor.onObjects(o))
    })

    // c.f. functor.withFinitelyGeneratedSource.withFinitelyGeneratedTarget.Pullback, which adds limitMorphism
  }

  lazy val pullback = new Pullback {}

}
