name := "categoricaldata web api"

organization := "net.categoricaldata"

version := "0.1.0"

scalaVersion := "2.9.2"

publishTo := Some(Resolver.sftp("tqft.net Maven repository", "tqft.net", "tqft.net/releases") as ("scottmorrison", new java.io.File("/Users/scott/.ssh/id_rsa")))
