package net.categoricaldata.ontology

import net.categoricaldata.category.FunctorsToSet

object Datasets extends FunctorsToSet {
  type O = Dataset
  type M = Datamap

  def internalize(f: net.categoricaldata.category.FunctorToSet) = {
    f.source match {
      case s: Ontology => s.functorsToSet.internalize(f)
    }
  }
  def internalize(t: net.categoricaldata.category.NaturalTransformationToSet) = {
    t.sourceCategory match {
      case s: Ontology => s.functorsToSet.internalize(t)
    }
  }
}
