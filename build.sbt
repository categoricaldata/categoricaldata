name := "categoricaldata"

organization := "net.categoricaldata"

version := "0.1.0"

// metaphor is trapped at 2.9.2
// it relies on bowler (which died during 2.9)
// 2.9.2 is the only version of the 2.9 series for which we have compatible published jars from toolkit 
scalaVersion := "2.9.2"

// scalaVersion := "2.10.1"

resolvers ++= Seq(
	"Java.net Maven2 Repository" at "http://download.java.net/maven/2/",
	"tqft.net Maven repository" at "http://tqft.net/releases",
	"Sonatype Nexus Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
	"Sonatype Nexus Releases" at "https://oss.sonatype.org/content/repositories/releases",
	"Scala Snapshots" at "http://scala-tools.org/repo-snapshots/"
)

// Project dependencies
libraryDependencies ++= Seq(
	"net.tqft" %% "toolkit-base" % "0.1.7",
	"net.tqft" %% "toolkit-collections" % "0.1.7",
	"net.tqft" %% "toolkit-arithmetic" % "0.1.7",
	"net.tqft" %% "toolkit-algebra" % "0.1.7",
	"net.tqft" %% "toolkit-amazon" % "0.1.7",
	"net.categoricaldata" %% "categoricaldata-web-api" % "0.1.0",
	"org.bowlerframework" % "core_2.9.1" % "0.5.1",
	"org.apache.httpcomponents" % "httpclient" % "4.1.1"
)

// lift-json
libraryDependencies ++= {
  val liftVersion = "2.4-M4" // Put the current/latest lift version here
  Seq(
    "net.liftweb" % "lift-json_2.9.1" % liftVersion % "compile->default",
    "net.liftweb" % "lift-json-ext_2.9.1" % liftVersion % "compile->default"
    )
}

// Test dependencies
libraryDependencies ++= Seq(
	"junit" % "junit" % "4.8" % "test,dev",
	"org.scalatest" % "scalatest_2.9.0" % "1.7.1" % "compile,test,dev"
)

// Dependencies for jetty
libraryDependencies ++= Seq(
	"org.mortbay.jetty" % "jetty" % "6.1.22" % "compile,container",
	"javax.servlet" % "servlet-api" % "2.5" % "provided" 
)

// Sometimes it's useful to see debugging out from the typer (e.g. to resolve slow compiles)
// scalacOptions += "-Ytyper-debug"

// Filters for 'test' and 'dev:test'
testOptions in Test := Seq(Tests.Filter(unitFilter))

testOptions in DevTest := Seq(Tests.Filter(devFilter))

// Setting up the metaphor webapp
seq(webSettings :_*)

port in container.Configuration := 8083

// We use method dependent types, which for now are an experimental feature. (But are in trunk, probably landing in 2.10.)
scalacOptions ++= Seq("-Xexperimental")

// make the stage task call the doc task, so the Scaladoc API pages are available on Heroku
stage <<= (stage in Compile) dependsOn (doc in Compile)

        scalacOptions in (Compile, doc) <++= (baseDirectory).map {
          (bd) =>
            val docSourceUrl = "https://bitbucket.org/categoricaldata/categoricaldata/raw/master€{FILE_PATH}.scala"
            Seq("-sourcepath", bd.getAbsolutePath, "-doc-source-url", docSourceUrl)
        }


// The Scala X-Ray plugin produces nicely formatted source code.
// addCompilerPlugin("org.scala-tools.sxr" %% "sxr" % "0.2.7")

// scalacOptions <+= scalaSource in Compile map { "-P:sxr:base-directory:" + _.getAbsolutePath }

publishTo := Some(Resolver.sftp("tqft.net", "tqft.net", "tqft.net/releases") as ("scottmorrison", new java.io.File("/Users/scott/.ssh/id_rsa")))

// add the "stage" task from the StartScriptPlugin, for heroku:
seq(com.typesafe.startscript.StartScriptPlugin.startScriptForClassesSettings: _*)

// specify the mainClass, for heroku
mainClass in Compile := Some("net.categoricaldata.server.Web")

// enable one-jar
seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)
