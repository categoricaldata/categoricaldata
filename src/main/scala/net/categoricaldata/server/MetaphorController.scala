package net.categoricaldata.server

import org.bowlerframework.controller.Controller
import org.bowlerframework.controller.FunctionNameConventionRoutes
import org.bowlerframework.Request
import net.categoricaldata.examples.Examples
import net.categoricaldata.ontology.Ontology
import net.categoricaldata.ontology.Dataset
import net.categoricaldata.ontology.Translation
import net.categoricaldata.ontology.Box
import com.thoughtworks.paranamer.CachingParanamer
import com.thoughtworks.paranamer.BytecodeReadingParanamer
import org.bowlerframework.view.Renderable
import org.bowlerframework.RequestScope
import net.categoricaldata.load.CSVLoader
import org.bowlerframework.http.BowlerHttpRequest

case class apiParameter(name: String, `type`: String)
case class apiHint(path: String, hasParameters: Boolean, parameters: List[apiParameter])
case class apiHints(hints: List[apiHint])

class MetaphorController extends Controller with FunctionNameConventionRoutes { metaphorController =>

  def redirect(to: String) = {
    val redirectPrefix = "/metaphor"
    RequestScope.response.sendRedirect(redirectPrefix + to)
  }

  // TODO when extracting from JSON, should store the original representation (partially implemented, c.f. OntologyTransformer et al.)

  // TODO automatically generate this glue for examples.
  def `GET /examples/ontologies/Chain`(n: Int): Ontology = Examples.Chain(n)
  def `GET /examples/ontologies/Isomorphism`: Ontology = Examples.Isomorphism
  def `GET /examples/ontologies/Graph`: Ontology = Examples.Graph
  def `GET /examples/ontologies/FiniteCyclicMonoid`(n: Int, k: Int): Ontology = Examples.FiniteCyclicMonoid(n, k)
  def `GET /examples/ontologies/Span`: Ontology = Examples.Span
  def `GET /examples/datasets/TerminalBigraph`: Dataset = Examples.TerminalBigraph
  def `GET /examples/translations/ReverseGraph`: Translation = Examples.ReverseGraph
  def `GET /examples/translations/Span_to_Chain2`: Translation = Examples.`Span_to_Chain(2)`

  def `GET /compute/leftPushforward`(translation: Translation, dataset: Dataset): Dataset = {
    translation.__!.onObjects(dataset)
  }
  def `GET /compute/rightPushforward`(translation: Translation, dataset: Dataset): Dataset = {
    translation.__*.onObjects(dataset)
  }
  def `GET /compute/pullback`(translation: Translation, dataset: Dataset): Dataset = {
    translation.^*.onObjects(dataset)
  }

  def `GET /compute/grothendieck`(dataset: Dataset): Translation = {
    dataset.grothendieck
  }
  def `GET /compute/yoneda`(ontology: Ontology, box: String): Dataset = {
    ontology.yoneda.onObjects(Box(box))
  }
  def `GET /compute/translationToDataset`(translation: Translation): Dataset = {
    translation.asPartialDataset.toDataset
  }

  def `GET /dataset/source`(dataset: Dataset): Ontology = dataset.source
  def `GET /translation/source`(translation: Translation): Ontology = translation.source
  def `GET /translation/target`(translation: Translation): Ontology = translation.target

  def `GET /load/dataset/CSV`(url: String): Dataset = CSVLoader(url)

  def `GET /help` = hints("")
  def `GET /examples` = hints("/examples")
  def `GET /examples/ontologies` = hints("/examples/ontologies")
  def `GET /examples/translations` = hints("/examples/translations")
  def `GET /examples/datasets` = hints("/examples/datasets")

  def `GET /passthru/ontology`(ontology: Ontology): Ontology = ontology
  def `GET /passthru/translation`(translation: Translation): Translation = translation
  def `GET /passthru/dataset`(dataset: Dataset): Dataset = dataset

  def `POST /store/ontology`(ontology: Ontology) = redirect("/store/ontology?hash=" + MetaphorStore.add(ontology))
  def `GET /store/ontology`(hash: String): Ontology = MetaphorStore.lookupOntology(hash)
  def `POST /store/translation`(translation: Translation) = redirect("/store/translation?hash=" + MetaphorStore.add(translation))
  def `GET /store/translation`(hash: String): Translation = MetaphorStore.lookupTranslation(hash)
  def `POST /store/dataset`(dataset: Dataset) = redirect("/store/dataset?hash=" + MetaphorStore.add(dataset))
  def `GET /store/dataset`(hash: String): Dataset = MetaphorStore.lookupDataset(hash)

  lazy val allHints = {
    val paranamer = new BytecodeReadingParanamer();
    apiHints(metaphorController.getClass().getDeclaredMethods().toList.filter({
      method => method.getName().startsWith("GET")
    }).map({
      method =>
        {
          val names = paranamer.lookupParameterNames(method).toList
          val types = method.getParameterTypes().toList.map(_.getName())
          val contextPath = RequestScope.request.asInstanceOf[BowlerHttpRequest].request.getContextPath
          apiHint(contextPath + method.getName().stripPrefix("GET$u0020").replaceAllLiterally("$div", "/"), names.nonEmpty, (names zip types).map(p => apiParameter(p._1, p._2)))
        }
    }))
  }
  def hints(filter: String) = apiHints(allHints.hints.filter(_.path.startsWith(filter)))
}