package net.categoricaldata.category

trait NaturalTransformation {
  val source: Functor
  val target: Functor
  lazy val sourceCategory: target.source.type = target.source
  lazy val targetCategory: target.target.type = target.target
  def apply(o: sourceCategory.O): targetCategory.M
}

trait ParametrizedNaturalTransformation[SC <: Category, TC <: Category] extends NaturalTransformation {
  override val source: ParametrizedFunctor[SC, TC]
  override val target: ParametrizedFunctor[SC, TC]
}
trait SourceParametrizedNaturalTransformation[SC <: Category, TC <: Category, SF <: ParametrizedFunctor[SC, TC]] extends ParametrizedNaturalTransformation[SC, TC] {
  override val source: SF
}
trait TargetParametrizedNaturalTransformation[SC <: Category, TC <: Category, TF <: ParametrizedFunctor[SC, TC]] extends ParametrizedNaturalTransformation[SC, TC] {
  override val target: TF
}
trait FullyParametrizedNaturalTransformation[SC <: Category, TC <: Category, SF <: ParametrizedFunctor[SC, TC], TF <: ParametrizedFunctor[SC, TC]] extends SourceParametrizedNaturalTransformation[SC, TC, SF] with TargetParametrizedNaturalTransformation[SC, TC, TF]
