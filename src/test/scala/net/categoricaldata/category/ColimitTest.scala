package net.categoricaldata.category

import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import net.categoricaldata.ontology._

@RunWith(classOf[JUnitRunner])
class ColimitTest extends FlatSpec with ShouldMatchers {
  // NOTE to use the DSL, you need this line:
  import net.categoricaldata.dsl.Sentences._

  val C = Ontology(objects = List("article", "author", "authorship"), arrows = List("authorship" --- "by" --> "author", "authorship" --- "has title" --> "article"))
  val i = Dataset(C,
    onObjects = Map("article" -> List("X", "Y", "Z"), "author" -> List("a", "b", "c", "d"), "authorship" -> List("Xa", "Xb", "Yb", "Yc", "Zd")),
    onMorphisms = Map(("authorship" --- "by" --> "author") -> (_.drop(1)), ("authorship" --- "has title" --> "article") -> (_.take(1))))

  "colimit" should "correctly find two connected components of the authorship graph" in {
    val colimit = i.colimitSet

    colimit.toIterable.size should equal(2)
  }

  "colimit and __!" should "agree for terminal functors" in { //FIXME (Scott) This test doesn't seem to be doing anything, but it should. The only thing I can think of is that there is nothing in Ontology#Dataset.
    //For any category C, and any dataset D:C-->Set, we should have colim(D)=TerminalFunctor(C).__!(D)
    for (
      dataset <- List[Ontology#Dataset]()
    ) {
      val C = dataset.source
      val T = Ontologies.morphismToTerminalObject(C)
      val x = T.target.objects.head // the target only has one object, so this hack to get it is okay.
      dataset.colimitSet should equal(T.__!.onObjects(dataset).onObjects(x))
    }
  }
  
}

